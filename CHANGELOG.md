# Changelog

## v1.4.1 (2025-02-14)

### Fixed

- Updated base image to `alpine:3.21.3`.

## v1.4.0 (2025-02-01)

### Changed

- Updated to `cloc` v2.04. See the
  [release notes](https://github.com/AlDanial/cloc/releases/tag/v2.04)
  for complete details.
- Updated `Dockerfile` to pull `cloc` directly from GitHub rather than `cloc`
  Docker Hub image, which is not consistently published. (#1)

## v1.3.1 (2025-01-09)

### Fixed

- Updated base image to `alpine:3.21.2`.

## v1.3.0 (2024-12-14)

### Changed

- Updated base image to `alpine:3.21.0`.

## v1.2.2 (2024-09-27)

### Fixed

- Fixed CI pipeline to update image `annotations` to be correct for this
  project.

## v1.2.1 (2024-09-06)

### Fixed

- Updated base image to `alpine:3.20.3`.

## v1.2.0 (2024-08-16)

### Changed

- Updated to `cloc` v2.02. See the
  [release notes](https://github.com/AlDanial/cloc/releases/tag/v2.02)
  for complete details.

## v1.1.2 (2024-07-24)

### Fixed

- Updated base image to `alpine:3.20.2`.

## v1.1.1 (2024-06-22)

### Fixed

- Updated base image to `alpine:3.20.1`.

## v1.1.0 (2024-05-22)

### Changed

- Updated base image to `alpine:3.20.0`.

### Miscellaneous

- Updated Renovate presets to v1.1.0.

## v1.0.0 (2024-03-24)

Initial release
