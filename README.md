# cloc

An Alpine-based container image to run [cloc](https://github.com/AlDanial/cloc)
to count lines of code. This is an optimized version of the original
[cloc image](https://hub.docker.com/r/aldanial/cloc) to allow more granular
dependency updates. The image also includes `jq` to facilitate manipulation of
JSON results.

## Install

`$ docker pull registry.gitlab.com/gitlab-ci-utils/container-images/cloc:latest`

All available container image tags can be found in the
`gitlab-ci-utils/container-images/cloc` repository at
<https://gitlab.com/gitlab-ci-utils/container-images/cloc/container_registry>.
The following container image tags are available:

- `vX.Y.Z`: Tags for specific releases (using semantic versioning). See the
  [release notes](https://gitlab.com/gitlab-ci-utils/container-images/cloc/-/releases)
  for details.
- `latest`: Tag corresponding to the latest commit to the `main` branch of
  this repository

**Note:** all other image tags in this repository, and any images in the
`gitlab-ci-utils/container-images/cloc/tmp` repository, are temporary images
used during the build process and may be deleted at any point.

## Usage

### GitLab CI example

The following is an example job from a `.gitlab-ci.yml` file to use this image
to count all code in the current directory with cloc.

```yml
cloc:
  image:
    name: registry.gitlab.com/gitlab-ci-utils/container-images/cloc:latest
    entrypoint: [""]
  stage: test
  needs: []
  script:
    - cloc .
```
