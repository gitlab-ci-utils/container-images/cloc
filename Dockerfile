FROM alpine:3.21.3@sha256:a8560b36e8b8210634f77d9f7f9efd7ffa463e380b75e2e74aff4511df3ef88c

ARG VERSION

# Install dependencies of the cloc apk package. The --depends option lists
# all dependent packages, and -q removes the header. Don't quote the
# `apk info` command since word splitting is required.
# hadolint ignore=DL3018,SC2046
RUN apk update &&       \
  apk add --no-cache jq $(apk info --depends -q cloc) && \
  rm -rf /var/cache/apk/* && \
  wget -q -O /usr/bin/cloc https://github.com/AlDanial/cloc/releases/download/v${VERSION}/cloc-${VERSION}.pl && \
  wget -q -O /usr/bin/cloc_LICENSE https://raw.githubusercontent.com/AlDanial/cloc/refs/tags/v${VERSION}/LICENSE && \
  chmod +x /usr/bin/cloc

WORKDIR /tmp

ENTRYPOINT ["cloc"]
CMD ["--help"]

LABEL org.opencontainers.image.licenses="MIT"
LABEL org.opencontainers.image.source="https://gitlab.com/gitlab-ci-utils/container-images/cloc"
LABEL org.opencontainers.image.title="cloc"
LABEL org.opencontainers.image.url="https://gitlab.com/gitlab-ci-utils/container-images/cloc"
